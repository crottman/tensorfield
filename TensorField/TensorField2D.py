'''TensorField2D.py supports 2x2 Tensor operations for PyCA'''

from TensorField import TensorField as _TensorField
from TensorField import Abs, Abs_I, Copy, MinMax, SetIdentity

import PyCA.Core as ca
import numpy as np
import os

import pycuda.driver as cuda
import pycuda.autoinit
from pycuda.compiler import SourceModule
# Compile and load CUDA file
_cbdir = os.path.dirname(__file__)
_f = open(os.path.join(_cbdir, 'TensorKernels.cu'), 'r').read()
_mod = SourceModule(_f, no_extern_c=True)


class TensorField2DException(Exception):
    '''Exception class for TensorField2D'''
    pass


class TensorField2D(_TensorField):
    '''TensorField2D class for PyCA that represents a field of 2D
    Symmetric Positive Definite Tensors:
    |m00 m01|
    |m01 m11|

    To create an entirely new Tensor, call
    TF = TensorField2D(grid, memType)

    To create a Tensor off of a list of 3 Image3Ds (with the same grid/memtype)
    TF = TensorField2D(Imlist=[Im00, Im01, Im11])
    '''

    def __init__(self, grid=None, memType=None, Imlist=None):
        if Imlist is None and grid is not None and memType is not None:
            self.elements = [ca.Image3D(grid, memType) for _ in xrange(3)]
            assert(isinstance(grid, ca.GridInfo))
            assert(isinstance(memType, int))
            self._grid = grid   # hidden so that we can make .grid a method
            self._memType = memType
        elif Imlist is not None:
            if len(Imlist) != 3:
                raise TensorField2DException("'elements' should be length 3")
            # self.elements = tuple(Imlist)
            self.elements = Imlist
            self._grid = self.elements[0].grid()
            self._memType = self.elements[0].memType()
        else:
            raise TensorField2DException("bad initialization: needs grid+memType or Imtuple")

        self.indices = tuple(((0,0), (0,1), (1,0), (1,1)))
        # dict that converst between the indices (int vs tuples)
        self.idxdict = {0:(0,0), 1:(0,1), 2:(1,1),
                        (0,0):0, (0,1):1, (1,0):1, (1,1):2}
        self.dim = 2


def Det(Imdet, M):
    '''Gives the determinant of a TensorField2D `M`.  The determinant is
    stored as an Image3D `Imdet`'''
    Imtmp = ca.Image3D(M.grid(), M.memType())
    ca.Mul(Imdet, M[0,0], M[1,1])
    ca.Mul(Imtmp, M[0,1], M[1,0])
    Imdet -= Imtmp


def Dot(out, A, B):
    if isinstance(A, TensorField2D) and isinstance(B, ca.Field3D):
        M, v = A, B
        Out_idx = ca.Image3D(M.grid(), M.memType())
        v_idx = ca.Image3D(M.grid(), M.memType())
        for i in xrange(2):
            ca.Copy(v_idx, v, 0)
            ca.Mul(Out_idx, M[i,0], v_idx)
            ca.Copy(v_idx, v, 1)
            ca.Add_Mul_I(Out_idx, M[i,1], v_idx)
            ca.Copy(out, Out_idx, i) # copy result back to 'out'
        ca.Copy(v_idx, v, 2)         # copy the 3rd component
        ca.Copy(out, v_idx, 2)
    elif isinstance(A, ca.Field3D) and isinstance(B, TensorField2D):
        Dot(out, B, A)
    elif isinstance(A, ca.Field3D) and isinstance(B, ca.Field3D):
        Aidx = ca.Image3D(A.grid(), A.memType())
        Bidx = ca.Image3D(A.grid(), A.memType())
        ca.Copy(Aidx, A, 0)
        ca.Copy(Bidx, B, 0)
        ca.Mul(out, Aidx, Bidx)
        ca.Copy(Aidx, A, 1)
        ca.Copy(Bidx, B, 1)
        ca.Add_Mul_I(out, Aidx, Bidx)
    else:
        raise TensorField2DException("Unknown input types for Dot")


def Inverse(TFinv, TF):
    '''compute the Matrix Inverse for each tensor in the tensor field'''
    if TFinv.memType() != TF.memType():
        raise TensorField2DException('Different memType!')
    if TF.memType() == ca.MEM_HOST:
        raise TensorField2DException('Not Yet Implemented')
    else:
        det = ca.Image3D(TF.grid(), TF.memType())
        Det(det, TF)
        ca.Div(TFinv[0,0], TF[1,1], det)
        ca.Div(TFinv[1,1], TF[0,0], det)
        ca.Div(TFinv[0,1], TF[0,1], det)
        ca.Neg_I(TFinv[0,1])

def Eig(EVec0, EVec1, EVal0, EVal1, TF):
    '''Computes Eig of a Tensor2D, putting eigenvectors in a list of 2
    Field3Ds and corresponding eigenvalues in a list of 2 Image3Ds.
    The eigenvalues are ordered from smallest to largest
    '''
    Imblock = (TF.size().x, 1, 1)
    Imgrid = (TF.size().y, TF.size().z, 1)
    Eig2DKernel = _mod.get_function("Eig2DKernel")

    if not (isinstance(EVec0, ca.Field3D) and isinstance(EVec1, ca.Field3D)) :
        raise TensorField2DException("EVecs should be Field3Ds")
    if not (isinstance(EVal0, ca.Image3D) and isinstance(EVal1, ca.Image3D)) :
        raise TensorField2DException("EVals should be Image3Ds")

    ca.SetToZero(EVec0)
    ca.SetToZero(EVec1)

    Eig2DKernel(np.uint64(EVec0.rawptr_x()),
                np.uint64(EVec0.rawptr_y()),
                np.uint64(EVec1.rawptr_x()),
                np.uint64(EVec1.rawptr_y()),
                np.uint64(EVal0.rawptr()),
                np.uint64(EVal1.rawptr()),
                np.uint64(TF[0,0].rawptr()),
                np.uint64(TF[0,1].rawptr()),
                np.uint64(TF[1,1].rawptr()),
                np.uint32(TF.size().x),
                np.uint32(TF.size().y),
                np.uint32(TF.size().z),
                block=Imblock,
                grid=Imgrid)
