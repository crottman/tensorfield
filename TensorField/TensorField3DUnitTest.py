import unittest
import TensorField3D as cc
import numpy as np
import PyCA.Core as ca
import PyCA.Common as common
from numpy.core.umath_tests import matrix_multiply

np.random.seed(62434536)

def AddSetUp(setUp, tearDown=None):
    def AddSetUpFunc(f):
        def AddSetUpWrappedFunc(*args,**kwargs):
            selfOb = args[0]
            if setUp != None:
                setUp(selfOb)
            f(*args,**kwargs)
            if tearDown != None:
                tearDown(selfOb)
        return AddSetUpWrappedFunc
    return AddSetUpFunc


def CheckField(v, varr):
    hv = v.copy()
    origType = hv.memType()
    hv.toType(ca.MEM_HOST)
    (vx,vy,vz) = hv.asnp()
    vnp = np.zeros(vx.shape + (3,))
    vnp[:,:,:,0] = vx
    vnp[:,:,:,1] = vy
    vnp[:,:,:,2] = vz
    diff = vnp - varr
    diffMax = np.max(np.abs(diff))
    diffAv = np.sum(np.abs(diff)) / (1.0*np.product(vx.shape)*3)
    hv.toType(origType)
    return (diffAv, diffMax)


def CheckTF(m, marr):
    origType = m.memType()
    m.toType(ca.MEM_HOST)
    diff = m.asnp()-marr
    diffMax = np.max(np.abs(diff))
    diffAv = np.sum(np.abs(diff) / np.prod(diff.shape))
    m.toType(origType)
    return (diffAv, diffMax)

def CheckImage(Im, Imarr):
    return CheckTF(Im, Imarr) # the asnp calls are the same


class NumpyTestCase(unittest.TestCase):

    # def __init__(self, methodName='runTest'):

    def setUp(self):
        self.sz = [5, 7, 9]
        self.grid = ca.GridInfo(ca.Vec3Di(self.sz[0], self.sz[1], self.sz[2] ))
        # self.memType = ca.MEM_HOST
        self.memType = ca.MEM_DEVICE

        # allowable average abs. diff
        self.AvEps = 1e-6
        # allowable max abs. diff
        self.MaxEps = 1e-4


    def genRandTFPair(self, spd=False, isotropic=False):
        randIms = [common.RandImage(self.sz, mType=self.memType) for _ in xrange(6)]
        randTF = cc.TensorField3D(Imlist=randIms)
        randTF.toType(ca.MEM_HOST)

        if isotropic:
            spd = True

        if spd:
            # make Tensors Symmetric Positive Definite
            ca.Abs_I(randTF[0,0])
            ca.Abs_I(randTF[1,1])
            ca.Abs_I(randTF[2,2])
            for (i, j, k) in np.ndindex(tuple(self.sz)):
                # if np.linalg.det(randTF.asnp()[i,j,k,:,:]) < .1:
                #     print 'too low'

                while np.linalg.det(randTF.asnp()[i,j,k,:,:]) < .1:
                    # print np.linalg.det(randTF.asnp()[i,j,k,:,:])
                    randTF[0,0].asnp()[i,j,k] += np.random.uniform(1, 3)
                    randTF[1,1].asnp()[i,j,k] += np.random.uniform(1, 3)
                    randTF[2,2].asnp()[i,j,k] += np.random.uniform(1, 3)

        randArr = randTF.asnp(copy=True)
        randTF.toType(self.memType)
        return (randArr, randTF)

    def genRandFieldPair(self):
        randF = common.RandField(self.sz)
        randFArr = np.zeros(self.sz + [3])
        (vx,vy,vz) = randF.asnp()
        randFArr[:,:,:,0] = vx
        randFArr[:,:,:,1] = vy
        randFArr[:,:,:,2] = vz
        randF.toType(self.memType)
        return (randFArr, randF)

    def randTF0SetUp(self, spd=False, isotropic=False):
        (self.randTFArr0, self.randTF0) = self.genRandTFPair(spd, isotropic)

    def randTF0TearDown(self):
        self.randTFArr0 = None
        self.randTF0 = None

    def randTF1SetUp(self):
        (self.randTFArr1, self.randTF1) = self.genRandTFPair()

    def randTF1TearDown(self):
        self.randTFArr1 = None
        self.randTF1 = None

    def resultTFSetUp(self):
        self.resultTF = cc.TensorField3D(self.grid, self.memType)

    def resultTFTearDown(self):
        self.resultTF = None

    def randFieldSetUp(self):
        (self.randFArr, self.randF) = self.genRandFieldPair()

    def randFieldTearDown(self):
        self.randFArr = None
        self.randF = None

    def resultFieldSetUp(self):
        self.resultField = ca.Field3D(self.grid, self.memType)

    def resultFieldTearDown(self):
        self.resultField = None

    def resultImSetUp(self):
        self.resultIm = ca.Image3D(self.grid, self.memType)

    def resultImTearDown(self):
        self.resultIm = None

    def TestTFsEqual(self, pycaTF, npTF, avEps=None, maxEps=None):
        if avEps is None:
            avEps = self.AvEps
        if maxEps is None:
            maxEps = self.MaxEps

        diffAv, diffMax = CheckTF(pycaTF, npTF)

        self.assertLess(diffAv, avEps)
        self.assertLess(diffMax, maxEps)

    def TestFieldsEqual(self, pycaF, npF, avEps=None, maxEps=None):
        if avEps is None:
            avEps = self.AvEps
        if maxEps is None:
            maxEps = self.MaxEps

        diffAv, diffMax = CheckField(pycaF, npF)

        self.assertLess(diffAv, avEps)
        self.assertLess(diffMax, maxEps)


    def TestEigEqual(self, EVec0, EVec1, EVec2, npEVecs,
                     EVal0, EVal1, EVal2, npEVals,
                     avEpsEVec=None, maxEpsEVec=None,
                     avEpsEVal=None, maxEpsEVal=None,
                     testVecs=None,
                     debug=False):

        if testVecs is None:
            testVecs = range(2) # which vectors to test

        sz = tuple(self.sz)
        EVec0.toType(ca.MEM_HOST)
        EVec1.toType(ca.MEM_HOST)
        EVec2.toType(ca.MEM_HOST)
        EVal0.toType(ca.MEM_HOST)
        EVal1.toType(ca.MEM_HOST)
        EVal2.toType(ca.MEM_HOST)

        if debug:
            print ''
            print 'PRE SORT'
            print 'Eigenvalues: (np, tf)'
            print npEVals[0,0,0,:],
            print [EVal0.asnp()[0,0,0], EVal1.asnp()[0,0,0], EVal2.asnp()[0,0,0]]
            print 'Eigenvectors: (np, tf)'
            print npEVecs[0,0,0,:]
            print np.array([[EVec0.asnp()[0][0,0,0], EVec1.asnp()[0][0,0,0], EVec2.asnp()[0][0,0,0]],
                            [EVec0.asnp()[1][0,0,0], EVec1.asnp()[1][0,0,0], EVec2.asnp()[1][0,0,0]],
                            [EVec0.asnp()[2][0,0,0], EVec1.asnp()[2][0,0,0], EVec2.asnp()[2][0,0,0]]])


        EVecs = [EVec0, EVec1, EVec2]

        SortednpEVecs = np.zeros(npEVecs.shape)
        SortednpEVals = np.zeros(npEVals.shape)

        for (i,j,k) in np.ndindex(sz[0:3]):
            # Move Evecs and Evals in the numpy arrays so that they are ordered
            idx = (-npEVals[i,j,k,:]).argsort() #ordered biggest to smallest
            # print idx
            for ii in range(3):
                SortednpEVecs[i,j,k,:,ii] = npEVecs[i,j,k,:,idx[ii]]
                SortednpEVals[i,j,k,ii] = npEVals[i,j,k,idx[ii]]
            # Since eigenvectors are equivalent if they are opposite
            # each other (and because i constrain eigenvectors to be
            # norm 1, we negate some of the numpy eigenvectors to make
            # them equal so we can pass them through TestFieldsEqual
            for ii in range(3):
                # currEVec = np.array([Evecs[ii].asnp()[0] # maybe later
                if (abs(EVecs[ii].asnp()[0][i,j,k] - SortednpEVecs[i,j,k,0,ii]) > .5 or
                    abs(EVecs[ii].asnp()[1][i,j,k] - SortednpEVecs[i,j,k,1,ii]) > .5 or
                    abs(EVecs[ii].asnp()[2][i,j,k] - SortednpEVecs[i,j,k,2,ii]) > .5):
                    SortednpEVecs[i,j,k,:,ii] *= -1 # flip

        if debug:
            print ''
            print 'POST SORT'
            print 'Eigenvalues: (np, tf)'
            print SortednpEVals[0,0,0,:],
            print [EVal0.asnp()[0,0,0], EVal1.asnp()[0,0,0], EVal2.asnp()[0,0,0]]
            print 'Eigenvectors: (np, tf)'
            print SortednpEVecs[0,0,0,:]
            print np.array([[EVec0.asnp()[0][0,0,0], EVec1.asnp()[0][0,0,0], EVec2.asnp()[0][0,0,0]],
                            [EVec0.asnp()[1][0,0,0], EVec1.asnp()[1][0,0,0], EVec2.asnp()[1][0,0,0]],
                            [EVec0.asnp()[2][0,0,0], EVec1.asnp()[2][0,0,0], EVec2.asnp()[2][0,0,0]]])


        for i in testVecs:
            self.TestFieldsEqual(EVecs[i], SortednpEVecs[:,:,:,:,i], avEpsEVec, maxEpsEVec)

        self.TestImsEqual(EVal0, SortednpEVals[:,:,:,0], avEpsEVal, maxEpsEVal)
        self.TestImsEqual(EVal1, SortednpEVals[:,:,:,1], avEpsEVal, maxEpsEVal)
        self.TestImsEqual(EVal2, SortednpEVals[:,:,:,2], avEpsEVal, maxEpsEVal)

        if debug:
            print 'Passed!'

        # Make sure eigenvectors are orthogonal
        zeroIm = np.zeros(sz)
        dotprod = ca.Image3D(EVec0.grid(), EVec0.memType())
        cc.Dot(dotprod, EVec0, EVec1)
        self.TestImsEqual(dotprod, zeroIm)
        cc.Dot(dotprod, EVec0, EVec2)
        self.TestImsEqual(dotprod, zeroIm)
        cc.Dot(dotprod, EVec1, EVec2)
        self.TestImsEqual(dotprod, zeroIm)

        EVec0.toType(ca.MEM_DEVICE)
        EVec1.toType(ca.MEM_DEVICE)
        EVec2.toType(ca.MEM_DEVICE)
        EVal0.toType(ca.MEM_DEVICE)
        EVal1.toType(ca.MEM_DEVICE)
        EVal2.toType(ca.MEM_DEVICE)

    def TestImsEqual(self, pycaIm, npIm, avEps=None, maxEps=None):
        if avEps is None:
            avEps = self.AvEps
        if maxEps is None:
            maxEps = self.MaxEps

        diffAv, diffMax = CheckImage(pycaIm, npIm)

        self.assertLess(diffAv, avEps)
        self.assertLess(diffMax, maxEps)


    ################################################################
    #
    # Begin Tests
    #
    ################################################################

    @AddSetUp(randTF0SetUp, randTF0TearDown)
    @AddSetUp(randTF1SetUp, randTF1TearDown)
    @AddSetUp(resultTFSetUp, resultTFTearDown)
    def test_Add(self):
        randC = -2.35439        # random value
        # Binary operator +

        self.resultTF = self.randTF0 + self.randTF1 # mat+mat
        npResult = self.randTFArr0 + self.randTFArr1
        self.TestTFsEqual(self.resultTF, npResult)
        self.resultTF = self.randTF0 + randC # mat+c
        npResult = self.randTFArr0 + randC
        self.TestTFsEqual(self.resultTF, npResult)
        self.resultTF = randC + self.randTF0 # c+mat
        npResult = randC + self.randTFArr0
        self.TestTFsEqual(self.resultTF, npResult)
        # inplace
        self.resultTF += self.randTF0 # mat += mat
        npResult += self.randTFArr0
        self.TestTFsEqual(self.resultTF, npResult)
        self.resultTF += randC # mat += c
        npResult += randC
        self.TestTFsEqual(self.resultTF, npResult)

    @AddSetUp(randTF0SetUp, randTF0TearDown)
    @AddSetUp(randTF1SetUp, randTF1TearDown)
    @AddSetUp(resultTFSetUp, resultTFTearDown)
    def test_Sub(self):
        randC = -2.35439        # random value
        self.resultTF = self.randTF0 - self.randTF1 # mat-mat
        npResult = self.randTFArr0 - self.randTFArr1
        self.TestTFsEqual(self.resultTF, npResult)
        self.resultTF = self.randTF0 - randC # mat-c
        npResult = self.randTFArr0 - randC
        self.TestTFsEqual(self.resultTF, npResult)
        # inplace
        self.resultTF -= self.randTF0 # mat -= mat
        npResult -= self.randTFArr0
        self.TestTFsEqual(self.resultTF, npResult)
        self.resultTF -= randC # mat -= c
        npResult -= randC
        self.TestTFsEqual(self.resultTF, npResult)

    @AddSetUp(randTF0SetUp, randTF0TearDown)
    @AddSetUp(randTF1SetUp, randTF1TearDown)
    @AddSetUp(resultTFSetUp, resultTFTearDown)
    def test_Mul(self):
        randC = -2.35439        # random value
        self.resultTF = self.randTF1 * self.randTF0 # mat*mat
        npResult = self.randTFArr1 * self.randTFArr0
        self.TestTFsEqual(self.resultTF, npResult)
        self.resultTF = self.randTF0 * randC # mat*c
        npResult = self.randTFArr0 * randC
        self.TestTFsEqual(self.resultTF, npResult)
        self.resultTF = randC * self.randTF0 # c*mat
        npResult = randC * self.randTFArr0
        self.TestTFsEqual(self.resultTF, npResult)

        # inplace - copy so they don't get too big
        cc.Copy(self.resultTF, self.randTF1)
        npResult = self.randTFArr1.copy()
        self.resultTF *= self.randTF0 # mat *= mat
        npResult *= self.randTFArr0
        self.TestTFsEqual(self.resultTF, npResult)

        cc.Copy(self.resultTF, self.randTF1)
        npResult = self.randTFArr1.copy()
        self.resultTF *= randC # mat *= c
        npResult *= randC
        self.TestTFsEqual(self.resultTF, npResult)

    @AddSetUp(randTF0SetUp, randTF0TearDown)
    @AddSetUp(randTF1SetUp, randTF1TearDown)
    @AddSetUp(resultTFSetUp, resultTFTearDown)
    def test_Div(self):
        randC = -2.35439        # random value
        cc.Abs_I(self.randTF0) # make denom not close to 0
        self.randTF0 += 0.1
        self.randTFArr0 = np.abs(self.randTFArr0) + 0.1

        self.resultTF = self.randTF1 / self.randTF0 # mat/mat
        npResult = self.randTFArr1 / self.randTFArr0
        self.TestTFsEqual(self.resultTF, npResult)
        self.resultTF = self.randTF1 / randC # mat/c
        npResult = self.randTFArr1 / randC
        self.TestTFsEqual(self.resultTF, npResult)
        # inplace
        self.resultTF /= self.randTF0 # mat /= mat
        npResult /= self.randTFArr0
        self.TestTFsEqual(self.resultTF, npResult)
        self.resultTF /= randC # mat /= c
        npResult /= randC
        self.TestTFsEqual(self.resultTF, npResult)

    @AddSetUp(randTF0SetUp, randTF0TearDown)
    @AddSetUp(randTF1SetUp, randTF1TearDown)
    @AddSetUp(resultTFSetUp, resultTFTearDown)
    @AddSetUp(resultFieldSetUp, resultFieldTearDown)
    @AddSetUp(randFieldSetUp, randFieldTearDown)
    def test_Dot(self):
        # TF (dot) TF is not necessarily Positive semidefinite (gen. not symmetric)
        # cc.Dot(self.resultTF, self.randTF0, self.randTF1) # mat (dot) mat
        # npResult = matrix_multiply(self.randTFArr0, self.randTFArr1)

        # self.TestTFsEqual(self.resultTF, npResult)

        cc.Dot(self.resultField, self.randTF0, self.randF) # mat (dot) vec
        # sz = self.randTFArr1.shape
        # npResult = np.zeros([sz[0], sz[1], sz[2], 3])
        # for (i, j, k) in np.ndindex(tuple(self.sz)):
        #     npResult[i,j,k,:] = np.dot(self.randTFArr0[i,j,k,:,:],
        #                                self.randFArr[i,j,k,:])
        # self.TestFieldsEqual(self.resultField, npResult)
        self.randFArr = self.randFArr.reshape(self.randFArr.shape + (1,))
        npResult = matrix_multiply(self.randTFArr0, self.randFArr).reshape(self.sz + [3])
        self.TestFieldsEqual(self.resultField, npResult)

        cc.Dot(self.resultField, self.randF, self.randTF0) # vec (dot) mat
        self.randFArr = self.randFArr.reshape(self.sz + [1, 3])
        npResult = matrix_multiply(self.randFArr, self.randTFArr0).reshape(self.sz + [3])
        self.TestFieldsEqual(self.resultField, npResult)

    @AddSetUp(resultImSetUp, resultImTearDown)
    @AddSetUp(randTF0SetUp, randTF0TearDown)
    def test_Det(self):
        cc.Det(self.resultIm, self.randTF0)
        sz = self.randTFArr0.shape
        npResult = np.zeros([sz[0], sz[1], sz[2]])
        for (i, j, k) in np.ndindex(tuple(self.sz)):
            npResult[i,j,k] = np.linalg.det(self.randTFArr0[i,j,k,:,:])
        self.TestImsEqual(self.resultIm, npResult)


    @AddSetUp(resultTFSetUp, resultTFTearDown)
    @AddSetUp(randTF0SetUp, randTF0TearDown)
    def test_Inverse(self):
        self.randTF0SetUp(spd=True, isotropic=False)

        cc.Inverse(self.resultTF, self.randTF0)
        sz = self.randTFArr0.shape
        npResult = np.zeros([sz[0], sz[1], sz[2], 3, 3])
        for (i, j, k) in np.ndindex(tuple(self.sz)):
            npResult[i,j,k,:,:] = np.linalg.inv(self.randTFArr0[i,j,k,:,:])

        # Loosen constraint because i'm not constraining this to be numerically stable (yet)
        self.TestTFsEqual(self.resultTF, npResult, avEps=1e-5, maxEps=1e-3)
        # self.TestTFsEqual(self.resultTF, npResult, avEps=1e-3, maxEps=1e-2)

    @AddSetUp(randTF0SetUp, randTF0TearDown)
    def test_Identity(self):
        self.randTF0.setIdentity()
        sz = self.randTFArr0.shape
        npResult = np.zeros([sz[0], sz[1], sz[2], 3, 3])
        for (i, j, k) in np.ndindex(tuple(self.sz)):
            npResult[i,j,k,:,:] = np.array([[1,0,0],[0,1,0],[0,0,1]])
        self.TestTFsEqual(self.randTF0, npResult)

    @AddSetUp(randTF0SetUp, randTF0TearDown)
    def test_Eig(self):
        self.randTF0SetUp(spd=True, isotropic=True)

        EVec0 = ca.Field3D(self.randTF0.grid(), self.randTF0.memType())
        EVec1 = ca.Field3D(self.randTF0.grid(), self.randTF0.memType())
        EVec2 = ca.Field3D(self.randTF0.grid(), self.randTF0.memType())
        EVal0 = ca.Image3D(self.randTF0.grid(), self.randTF0.memType())
        EVal1 = ca.Image3D(self.randTF0.grid(), self.randTF0.memType())
        EVal2 = ca.Image3D(self.randTF0.grid(), self.randTF0.memType())
        cc.Eig(EVec0, EVec1, EVec2, EVal0, EVal1, EVal2, self.randTF0)
        sz = self.randTFArr0.shape
        npResultEVecs = np.zeros([sz[0], sz[1], sz[2], 3, 3])
        npResultEVals = np.zeros([sz[0], sz[1], sz[2], 3])
        for (i,j,k) in np.ndindex(sz[0:3]):
            npResultEVals[i,j,k,:], npResultEVecs[i,j,k,:,:] = np.linalg.eigh(self.randTFArr0[i,j,k,:,:])
        # Test Equality
        self.TestEigEqual(EVec0, EVec1, EVec2, npResultEVecs,
                          EVal0, EVal1, EVal2, npResultEVals)

        # test identity matrix
        cc.SetIdentity(self.randTF0)
        for (i,j,k) in np.ndindex(sz[0:3]):
            self.randTFArr0[i,j,k,:,:] = np.array([[1, 0, 0],[0, 1, 0],[0, 0, 1]])
        cc.Eig(EVec0, EVec1, EVec2, EVal0, EVal1, EVal2, self.randTF0)
        for (i,j,k) in np.ndindex(sz[0:3]):
            npResultEVals[i,j,k,:], npResultEVecs[i,j,k,:,:] = np.linalg.eigh(self.randTFArr0[i,j,k,:,:])
        # Test Equality
        self.TestEigEqual(EVec0, EVec1, EVec2, npResultEVecs,
                          EVal0, EVal1, EVal2, npResultEVals)

        # Test another degenerate situation 201-010-102 matrix
        # eigenvalues should be 1,1,3
        cc.SetIdentity(self.randTF0)
        ca.SetMem(self.randTF0[0,0], 2)
        ca.SetMem(self.randTF0[2,2], 2)
        ca.SetMem(self.randTF0[0,2], 1)
        for (i,j,k) in np.ndindex(sz[0:3]):
            self.randTFArr0[i,j,k,:,:] = np.array([[2, 0, 1],[0, 1, 0],[1, 0, 2]])
        cc.Eig(EVec0, EVec1, EVec2, EVal0, EVal1, EVal2, self.randTF0)
        for (i,j,k) in np.ndindex(sz[0:3]):
            npResultEVals[i,j,k,:], npResultEVecs[i,j,k,:,:] = np.linalg.eigh(self.randTFArr0[i,j,k,:,:])
        # only the unique (largest) eigenvalue need be correct, the rest should just be orthogonal
        # some numerical issues with this, might look into later, for now ease on constraint
        self.TestEigEqual(EVec0, EVec1, EVec2, npResultEVecs,
                          EVal0, EVal1, EVal2, npResultEVals,
                          maxEpsEVal=1e-3, avEpsEVal=1e-3, testVecs=[0])
        # Test another degenerate situation 201-030-102 matrix
        # eigenvalues should be 1,3,3
        cc.SetIdentity(self.randTF0)
        ca.SetMem(self.randTF0[0,0], 2)
        ca.SetMem(self.randTF0[1,1], 3)
        ca.SetMem(self.randTF0[2,2], 2)
        ca.SetMem(self.randTF0[0,2], 1)
        for (i,j,k) in np.ndindex(sz[0:3]):
            self.randTFArr0[i,j,k,:,:] = np.array([[2, 0, 1],[0, 3, 0],[1, 0, 2]])
        cc.Eig(EVec0, EVec1, EVec2, EVal0, EVal1, EVal2, self.randTF0)
        for (i,j,k) in np.ndindex(sz[0:3]):
            npResultEVals[i,j,k,:], npResultEVecs[i,j,k,:,:] = np.linalg.eigh(self.randTFArr0[i,j,k,:,:])
        # only the unique (smallest) eigenvalue need be correct, the rest should just be orthogonal
        self.TestEigEqual(EVec0, EVec1, EVec2, npResultEVecs,
                          EVal0, EVal1, EVal2, npResultEVals,
                          testVecs=[2])

        # Another Test Case - diagonal matrix
        cc.SetIdentity(self.randTF0)
        ca.SetMem(self.randTF0[0,0], 3.3)
        ca.SetMem(self.randTF0[1,1], 2.7)
        ca.SetMem(self.randTF0[2,2], 5.5)
        for (i,j,k) in np.ndindex(sz[0:3]):
            self.randTFArr0[i,j,k,:,:] = np.array([[3.3, 0, 0],[0, 2.7, 0],[0, 0, 5.5]])
        cc.Eig(EVec0, EVec1, EVec2, EVal0, EVal1, EVal2, self.randTF0)
        for (i,j,k) in np.ndindex(sz[0:3]):
            npResultEVals[i,j,k,:], npResultEVecs[i,j,k,:,:] = np.linalg.eigh(self.randTFArr0[i,j,k,:,:])
        self.TestEigEqual(EVec0, EVec1, EVec2, npResultEVecs,
                          EVal0, EVal1, EVal2, npResultEVals)
        # Another diagonal matrix
        cc.SetIdentity(self.randTF0)
        ca.SetMem(self.randTF0[0,0], 5.5)
        ca.SetMem(self.randTF0[1,1], 2.7)
        ca.SetMem(self.randTF0[2,2], 4.4)
        for (i,j,k) in np.ndindex(sz[0:3]):
            self.randTFArr0[i,j,k,:,:] = np.array([[5.5, 0, 0],[0, 2.7, 0],[0, 0, 4.4]])
        cc.Eig(EVec0, EVec1, EVec2, EVal0, EVal1, EVal2, self.randTF0)
        for (i,j,k) in np.ndindex(sz[0:3]):
            npResultEVals[i,j,k,:], npResultEVecs[i,j,k,:,:] = np.linalg.eigh(self.randTFArr0[i,j,k,:,:])
        self.TestEigEqual(EVec0, EVec1, EVec2, npResultEVecs,
                          EVal0, EVal1, EVal2, npResultEVals)


suite = unittest.TestLoader().loadTestsFromTestCase(NumpyTestCase)
unittest.TextTestRunner(verbosity=2).run(suite)
